function drawScene(gl, programInfo, buffers, vertexCount) {
	// Prepare matrix calculations library
    const mat4 = glMatrix.mat4;

	// Clear the world previous graphics and depth buffer
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// Prepare the camera viewport and projection matrix
    const fieldOfView = 90 * Math.PI / 180;   // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();
    mat4.perspective(projectionMatrix,
        fieldOfView,
        aspect,
        zNear,
        zFar);

	// Prepare the world model view matrix
    const modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [-0.0, 0.0, -6.0]);

	{
		// Link the vertex positions matrix to the program predefined vertex position matrix
		const numComponents = 2;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
		gl.vertexAttribPointer(
			programInfo.attribLocations.vertexPosition,
			numComponents,
			type,
			normalize,
			stride,
			offset);
		gl.enableVertexAttribArray(
			programInfo.attribLocations.vertexPosition);

		gl.useProgram(programInfo.program);
	}

	// Transform projection matrix of the program
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix);
	// Transform model matrix of the program
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix);

	{
		const offset = 0;
	
		// Draw line strips using the verticies to the drawing buffer
		gl.drawArrays(gl.LINE_STRIP, offset, vertexCount);
	}
}

function drawLines() {
	// Get the canvas element
    const canvas = document.getElementById('glCanvas');
	// Get the Web GL context of the element to draw on it
    const gl = canvas.getContext('webgl');


	// If invalid Web GL context fail safe
    if (!gl) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.');
        return;
    }

	/*
	 * Code source for the vertex shader
	 * Controls view and projection matrix, and creating vertex mesh
	 */
    const vsSource = `
    attribute vec4 aVertexPosition;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    }
  `;

	/*
	 * Code source for the fragment shader
	 * Controls coloring and interpolation of the fragments, materials,
	 * and other fragments related graphics
	 */
    const fsSource = `
    void main() {
      gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
  `;

	// Predefined example code to create a shader, defined by Web GL
	
	// Creating compiled vertex shader machine code
    const vertexShader = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShader, vsSource);

    gl.compileShader(vertexShader);

    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(vertexShader));
        gl.deleteShader(vertexShader);
        return null;
    }

	// Creating compiled fragment shader machine code
    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShader, fsSource);

    gl.compileShader(fragmentShader);

    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(fragmentShader));
        gl.deleteShader(fragmentShader);
        return null;
    }

	/* 
	 * Predefined code example to create Web GL shader program
	 * to run on the Web GL GPU device
	 */
    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return null;
    }

	// Capturing pointers to the Vertex Mesh, Projection Matrix and Model View Matrix
    const programInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
        },
    };

	// Defining number of verticies in the Vertex Mesh
    const vertexCount = 50;

	// Creating processing buffer for the verticies
    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

	// Filling in the verticies position
    let positions = [];
    for(var i = 0; i < vertexCount; i++){
        let x = 5 * 2.0 * (Math.random() - 0.5);
        let y = 2 * 2.0 * (Math.random() - 0.5);
        positions.push(x);
        positions.push(y);
    }

	// Preparing buffer for processing
    gl.bufferData(gl.ARRAY_BUFFER,
        new Float32Array(positions),
        gl.STATIC_DRAW);

    buffers = {
        position: positionBuffer,
    };

	/*
  	 * Draw the verticies buffer in the defined 
	 * world model using defined projection view
	 */
    drawScene(gl, programInfo, buffers, vertexCount);
}

drawLines();
